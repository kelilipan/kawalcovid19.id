export { default as VaccinateSituationSection } from './VaccinateSituationSection';
export { default as TahapanVaksinasi } from './TahapanVaksinasi';
export { default as ResearchSection } from './ResearchSection';
export { default as CandidateTable } from './CandidateTable';
